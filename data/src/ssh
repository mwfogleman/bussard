-- This -*- lua -*- defines a mode based on console mode, but for communicating
-- with another OS over an SSH connection.

-- initiate the connection. command may be nil.
ssh = function(username, password, command)
   ship.editor.change_buffer("*console*")
   if(ship.status.target and ship.status.target.os) then
      ssh_connect(username or "guest", password or "", command)
      ship.editor.end_of_buffer()
      ship.editor.no_mark()
      ship.editor.activate_mode("ssh")
   else
      print("Cannot log into target.")
   end
   return ship.editor.invisible
end

portal = function()
   if(ship.status.target and ship.status.target.portal) then
      ssh_connect("guest", "")
   else
      print("Cannot activate portal.")
   end
   return ship.editor.invisible
end

local get_input = function()
   return utf8.sub(ship.editor.get_line(0),
                   utf8.len(ship.editor.get_prompt()) + 1)
end

define_mode("ssh", "console") -- inherit bindings from console

-- send input to whatever remote OS you're connected to.
bind("ssh", "return", function()
        local input = lume.trim(get_input())

        if(input == "logout") then
           ship.editor.newline()
           logout()
        else
           ship.editor.history_push(input)
           ssh_send_line(input)
           ship.editor.newline(2)
           ship.editor.print_prompt()
           ship.editor.end_of_buffer()
           ship.editor.no_mark()
        end
end)

bind("ssh", "ctrl-d", function()
        if(get_input() == "") then
           logout()
           print("Logged out.")
        else
           ship.editor.delete_forwards()
        end
end)
