From: strauss@katilaycollege.edu.katilay
To: captain@adahn.local
Date: 2386:18,575,241
Content-Type: text/plain; charset=UTF-8
Subject: Forgive me
Message-Id: 124ced33-46eb-48f8-a3e6-e22e54202336

So, you found this. Not exactly a secure channel, but your ship is about
to launch, and everyone else is busy enough with preparations that it
will likely go unnoticed.

On some level I do believe they are right... this was the only way. But
for us to send you off like this, completely unaware of what you're
going into, and more importantly unaware of where you've been... it
isn't fair to you. It's too late to undo what we've decided. The wheels
are set in motion; the ship will launch, and that part at least is for
the best.

But you deserve better, so I've done what I can here. I can't bring back
your memories, but I discovered a backup in the lab of some of your
journal files. They're encrypted, and I don't know what they contain,
but my hope is that they will help you understand what's happened to
you, where you've come from, and where your destiny lies. You can find
them in the "docs.backup" directory.

There's an old, old story I heard once about a man who came to his
senses on a remote road where an old woman stood before him. "And what
is your third wish?", she asked him. "My third wish?" he replied. "But I
haven't made a first or second!" She told him that his second wish had
been to change things back to the way they had been before his first
wish, leaving him with no memory of either. After some thought, he
responded that he wished to know who he was. "That's funny," said the
woman, as she complied. "That was your first wish."

I don't know what lies in store for you, but I hope that with time you
will come to forgive us.

Yours,
Prof. Strauss
