# Ceres Shipyards Janus Cargo Bay

The Ceres Shipyards Janus Cargo Bay increases the cargo capacity of
your ship by a further 64 tons.

Since it is a passive upgrade, no configuration or activation is
necessary to take advantage of its functionality.

Copyright © 2402 Ceres Shipyards, All Rights Reserved.
