# Challenges

The progression is fairly linear up to the point where you get access
to the Spacetime Junction, at which point it branches out with a
number of "failed timelines" that you explore, and then reset back to
the Junction point till you know enough to get to LHS 451 and finish
the game.

There will be coding challenges. They can't be too contrived. Few of
them should involve writing from scratch. In real life it's much more
common to scrounge together solutions from existing snippets you find.

## Required

* decrypt journals
* SQL injection (to get off the portal blacklist?)
* light curve analysis of telescope data (to find which system Traxus colonized)
* password hash dictionary attack
* learn scheme
* learn forth

## Optional

* Cargo pricing tracker
* Auto-pilot

## Not sure?

* Sokoban-style thing?
* Reconnecting power
* Add logger to an account with a shared password
* Buffer overflow
