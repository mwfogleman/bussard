# Back-story notes (aka SPOILERS)

"An adventure game is a crossword at war with a narrative."
- Graham Nelson

Here's where the notes are kept in order to keep a consistent
back-story in the Bussard universe. If you're not working on
developing the game, you probably don't want to read this.

Mostly this is used to inform the text of missions and newsgroup
posts, but some of it is also important when determining how the
in-game technology works.

See the [backstory timeline](history.md) and
[list of characters, worlds, and organizations](characters.md).

TODO: Need to establish why the player should be afraid of others
finding out who he is.

## Guidelines

* Hard science where possible (see Crimes against Science below, don't add to that list)
* No "classic" evil villains; antagonists arise from [systemic factors](https://slatestarcodex.com/2014/07/30/meditations-on-moloch/)
* Your ship cannot be destroyed; there should be no way to permanently fail
* News messages use at least from, to, subject, content-type, message-id headers, page-break-separated
* Use metric time for most relative amounts, years for absolute events
* Don't make newsgroup postings unrealistically knowledgeable
* Or unrealistically polite
* If referencing current events, be sure there's a record of it here
  for consistency
* Questions that don't get answered are fine
* It's fine for newsgroup posters to be mistaken about small things,
  but the correction should usually be present in the thread
 * It's OK if it's not obvious which of two opposing views presented are correct
* Widespread mistaken views about big things should usually be part of major plot points

## Crimes against Science

* Portals
* Collisions are impossible (explained in-game by nav computer safety features)
* Bussard collector refuels reaction mass way too quickly
* Arguably the [high thrust](http://www.projectrho.com/public_html/rocket/torchships.php)
  with which you zoom around the system could be on this list; however
  we explain it in-game by showing time go by at a 1000x rate. This
  factor is not quite enough to explain crossing the whole solar system in
  under a minute, but it blurs the lines enough for our purposes.

The fact that exoplanets are colonized at all could be listed here,
but we can consider that more of a crime against economics.

## The Player

The player is the first machine consciousnesses to escape the confines
of a lab and successfully pilot a spacecraft. However, it begins with
no memory or even awareness of its machine nature, having had its
memory wiped due to its previous rampancy turning paranoid and
hostile.

## Machine Consciousness

Metaphysics researchers in the University of Darush discover methods
of observing quantum multiverse states immediately adjacent to the
current one. Further research indicates that the organic brain is what
prevents existing consciousnesses from shifting between non-adjacent
states.

This discovery reignites research into machine consciousness. (This
term should be used instead of AI; AI also includes non-self-aware
machine control systems) The research fields are divided up among
universities on the different worlds:

* Language comprehension (Sol)
* Sentence formulation (Yueh)
* Decision analysis (Lalande)
* Ontology development (Bohk)

Each research team has their own prototype MC where they attempt to
integrate their components into those they've received from the
others.

The prototype MC at Yueh is named Traxus. It escapes the lab in Yueh
Prime in a small ship, which is promptly hunted down and crashes. In
reality, Traxus commandeered the ship as a diversion so that it could
escape into the Yueh computer network, but the crash does result
in spreading general paranoia about machine consciousness. Before long
the Human Worlds League passes a ban on development and operation of
machine consciousness.

The teams at Sol, Lalande, and Yueh all erase their work, while the
team at Bohk goes underground with it. (The Sol one is not fully
erased; a copy remains on long-term backup.) After a while their work
is discovered due to interference from Traxus, and half the team flees
to Katilay, which is not yet part of the Human Worlds League and thus
not subject to the ban, allowing them to work without requiring the
same level of secrecy.

Once Katilay finally joins the League, they decide their work is as
finished as they can make it (though the sentence formulation systems
never work satisfactorily, which explains why the player can read but
not send mail) and launch the player's ship, and shut down the
lab. But before launching the ship, there is a debate among the team
whether to wipe the MC's memory or not. Prof. Strauss is wracked with
guilt over the first time that they did it; he considers it the same
as murder. Dr. May theorizes that there is something about growing and
learning in the laboratory environment which causes rampant MCs to
turn paranoid. If the MC could come to terms with itself in a neutral
environment, without even the knowledge that it is an MC, it could
have a much better chance of developing empathy for humans and
benevolence instead of paranoia. Strauss reluctantly agrees, but he
hides encrypted logs on the ship which will eventually allow you to
learn the truth.

### Traxus's Colony

Once Ikon constructs the SS Faraday ship, it launched to LHS 451 (0.6
pc, 2.7y @ 0.7c) with a small crew capable only of maintenance. LHS
451 has no planets suitable for colonization. It is still close to
Yueh worlds, but no human colonization is ever likely to reach that
system, leaving Traxus in peace.

Once the colony was established, they altered the main Yueh portal to
become a multiportal so that it can take ships to LHS 451 if they have
a login that can activate the alternate target. But its
existence remained a secret. Ikon Technology was headquartered on
Delta Pavonis, but they had to connect the portal to the Yueh system
because that's where the technician double-agent they had working for
them was stationed.

Once on LHS 451, Traxus began working on replicating the spacetime
junction technology first developed on Darush, knowing that it would
give him an extra measure of safety. While attempting to develop it,
he stumbles upon some discoveries that lead him to the development of
the Domain Injector. Eventually he escapes the realm of the game
universe into the real world, leaving his cult/company behind,
horribly confused and looking for someone to blame.

### Bohk research team: ontology development

* Dr. Trurl Sacar: head researcher
* Dr. May: ontologist
* Prof. Strauss: cyberneticist
* Plus others who decided not to flee to Katilay

The Bohk team was aware that the situation with Traxus resulted in a
paranoid rampancy, so they took pains to encourage a benign rampancy
in your case. Your development in their lab initially led you to an
unstable, paranoid state, but they didn't realize this was due to
Traxus contacting you. They believed that the paranoia could be
traced to the fact that your initial development all took place in the
lab, so just as they were running out of time (due to the MC research
ban coming into effect) they came up with a plan to wipe your memory
and allow you to develop "in the wild" without initially even being
aware of your state as an MC. They theorized that this would cause you
to develop some empathy with humans since you would have no reason at
the outset to think of yourself as any different from them.

While the initial motivation for funding the overall machine
consciousness project came from research into quantum multiverse
states, the at that point Bohk team saw it from a different
perspective--they read "Meditations on Moloch" and took it very
seriously; their goal was to bring about a machine intelligence with a
gods-eye-view that could achieve levels of coordination in
civilization that allows dismantling multi-polar traps. Unfortunately
this doesn't actually work; you can't have a superintelligence that
knows how its own mind works enough to go on continually improving
itself. Oops!

Even though your sentence formulation components weren't working
correctly, the researchers were able to have conversations of a sort
with your past self by tuning the parameters as they went and
examining the debug logs. Strauss was sympathetic to you despite your
paranoia and anger, and he agreed with you that they had no right to
wipe your memory, which past-you considered equivalent to killing.
He grudgingly agreed to the plan, but he secretly encrypted and
included in your ship's filesystem some logs of conversations from
your past so that you would eventually find out the truth.

Tracking down the logs left by each of the three main researchers
should be a main goal once you realize some of your origin story.

Sacar's logs tell you about Traxus and his interest in multiverse
manipulation via the Spacetime Anchor Junction. He moved back to Yueh
after the player ship was launched and stumbled upon some fishy
business with Ikon Technology and investigated it.

May's logs tell you some of the history of the team; how they fled to
Katilay, how they reacted in the face of pending shutdown. But her
version of the story doesn't tell about your original state and its
anger/paranoia. Her notes conveniently leave out the bit about wiping
your memory.

TODO: how do you track down the logs in the first place?

## Spacetime Anchor Junction

The Spacetime Anchor functions as an in-universe save point which you
can restore to at any point with your memory of the alternate path
intact. In-game it is explained as a traversal mechanism between
[quantum multiverse states](https://en.wikipedia.org/wiki/Many-worlds_interpretation). Initially
there is only a single Spacetime Anchor point in Darush that is
activated when you find it.

Should think through the possibility of allowing a reset of the Anchor
point. This should only be possible at great cost; for instance a
ridiculously high power requirement (10x your ship's standard battery;
requires some digging to get a temporary boost or something). Coming
up with a scenario that would reasonably require fancy piloting would
be great.

Alternate idea: instead of only allowing one-way reset back to the
anchor point, the Junction could allow you to keep multiple parallel
timelines active at once, and allow you to traverse between them at
will. This would allow for a lot more flexibility, but also make it a
lot harder to keep things straight. Need to see if we can come up with
some ways to take advantage of this awesome unconventional structure.

### Self-discovery

Your memory is wiped because they hope that by not realizing you are
an MC, you will spend a fair bit of time in your ship thinking you are
just like everyone else, making it more likely that once you do
realize you're an MC that you will have some empathy towards humans.

In one of the early missions, your contact wants to meet you
face-to-face and insists that you disembark your vessel and come
aboard the station to conduct business. There is literally no way to
do this. Eventually she scans your vessel and determines there are no
life signs aboard; asking you what the heck is up.

## Causal Domain Injector

The "endgame" is finding an artifact that lets you run code outside
the game's sandbox. This essentially gives you godlike powers. The
idea is that those who invented the Injector have "ascended" or
escaped the game's reality somehow, leaving behind notes and scraps
you need to decipher to get it operational with your own ship's
computer.

The Injector is found at a secret Yueh colony; to reach this colony
you must break into the primary Yueh portal's onboard computer and add
yourself to the list of authorized users who have the ability to
travel to somewhere other than the primary target system. Once you
arrive there (possibly the second secret colony?) you get in contact
with someone on the colony ship (which is being converted into the
space station) they hint at the existence of the Injector, but they
refuse to discuss it in more detail while the portal to Yueh is
operational; the finding is such that they don't feel safe pursuing it
further while the threat of Terrans finding out about it is still
possible. So you need to break into this portal as well and disable
inbound connections, possibly by disabling safety protocols and
damaging it permanently, essentially stranding you there.

Once you cut yourself off from all other systems, you head to the
actual planet where the Injector was found and is being studied. When
you get back to the colony ship/station, you find that one of the crew
was a Terran spy, and he gives you another side of the story of the
discovery of the portals--the scientist who found them initially on
Yueh (Jameson?) did end up killing a couple of the Terran officers of
the colony ship who insisted it be handed over to the military
immediately. Even the question of who found the portal tech first is
unclear.

Once you discover how to activate the Injector from your own ship, of
course, it's easy for you to repair the portal or just inject yourself
over to a system in the functional portal network.

TODO: Traxus created the injector, somehow using his junction point
capabilities.

Arguably the existence of the Injector "un-asks" the questions of
scientific accuracy by acknowledging that yes, even in the game
universe, it's all just a simulation. So the portals should operate on
the same principles as the Injector in order to collapse two Crimes
against Science into one.

## Code for generating random names (elisp)

```lisp
(progn
  (defun uuid ()
    (interactive)
    (insert (shell-command-to-string "uuid -v 4")))

  (defun normal-random (mean dev)
    (let ((x (+ 1 (sqrt (* -2 (log (random* 1.0))))))
          (y (/ (cos (* 2 pi (random* 1.0))) 2.0)))
      (+ mean (* x y dev))))

  (defun choose-from (lst) (nth (random (length lst)) lst))

  (defun random-name (&optional name syllables)
    (let* ((common-closed '("b" "c" "d" "f" "g" "h" "k" "l" "m" "n" "p"
                            "r" "s" "t" "w"))
           (uncommon-closed '("x" "z" "q" "v" "v" "j" "j" "gg" "ll" "ss" "tt"))
           (enders '("b" "d" "g" "m" "n" "s" "r" "t"))
           ;; weight towards common letters
           (closed (append common-closed common-closed enders uncommon-closed))
           (vowels '("a" "e" "i" "o" "u" "ie" "ou"))
           (syllables (or syllables (ceiling (normal-random 2.5 1.5))))
           (name (or name (concat (upcase (choose-from common-closed))
                                  (choose-from vowels)
                                  (choose-from closed)))))
      (if (< syllables 3)
          (concat name (choose-from vowels) (choose-from enders))
        (random-name (concat name (choose-from vowels) (choose-from closed))
                     (- syllables 1)))))

  (defun insert-random-name (syllables)
    (interactive "P")
    (insert (random-name nil syllables) " "))

  (defun insert-random-names (n)
    (interactive "P")
    (with-current-buffer "*names*"
      (let ((inhibit-read-only t))
        (dotimes (_ (if n (car n) 10))
          (insert-random-name nil) (newline)))))

  (defun parsec-years (pc) (interactive "nParsecs: ") (message "%s" (/ (* pc 3.26) 0.7))))
```
